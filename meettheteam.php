    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
  
      

         

        </head>

        <body>
  <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 
   
  <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>



    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>



            <div class = "mike">
            <img src = "mike.jpg" alt = "Mike" ></div>
            <div class = "mikeheader"><h1>Michael Penny</h1></div>
        <section class = "mikebio"><p>Michael is the Owner and Head Trainer of Delta Prime Fitness. A Caroline County native, he has trained in the Ladysmith and Fredericksburg area for the last 5 years and has spent his time working with a varied group of individuals, ages and genders. Specializing in large group training, one-on-one training and Sport Specific training.</p>
            <u> <h3>Education/Certifications:</h3> </u>
            <ul><li>BS Physical Education, Wellness, and Sports Science Averett University, Danville, Virginia
                </li>
            <li>National Strength and Conditioning Association (NSCA) Certified Personal Trainer </li>
            <li>First Aid, CPR/AED </li></ul>
            
            <u> <h3>Fun Facts:</h3></u>
            
        <p><B>Favorite Cheat Meal: </B>Donuts, Ice cream, and anything to satisfy the sweet tooth, of course, in trying to stay mindful of moderation!</p>
        <p><B>Quote:</B>“Be Humble. Be Hungry. And always be the hardest worker in the room.” Dwayne Johnson</p>
        <p><B>Hobbies: </B>Anything involving my 3 kids. I enjoy anything outdoors, and I really love to hunt and fish. I enjoy working, lifting, and spending time with my family either out on the boat or simply playing corn hole in the front yard.</p>
        <p><B>Why: </B>I do what I do because I love to see the joy in people’s faces as they conquer goals and achievements that they never thought possible. I thrive to see progress, both mentally and physically in everyone I train. Whether it’s a young athlete or an Active Adult looking to be healthier, I enjoy showing people that their efforts and dedication will pay off.
</p>
        <p><B>Favorite Equipment: </B>I enjoy utilizing abstract things like sandbags, tractor tires, hay bales, logs, etc. These items are used outside where the fresh air ignites the beast inside of everyone, and we aren’t confined to four walls. Inside of the wellness facility, I would say my favorite equipment would the barbell. I admire the pure power the body generates to lift a loaded barbell, whether its doing deadlifts, power cleans or squats.
   
</p>
        <p><B>Ultimate Fitness Goal: </B>To be a hero to my children and to show everyone that you can be strong and fit and still live a life outside of hours upon hours in the gym. I ultimately want people to realize their bodies can change and still live a normal life with family and friends. 

</p>
            </section>
            
            

      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </body>




<?php
include 'footer.php';
?>




    </HTML>

