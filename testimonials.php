    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
  
      

         

        </head>

        <body>
         <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 
    
  <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

    <div class="header"><h2>Testimonials</h2></div>
      <div class="containertestimonials">
	<div class="row">
		<div class="col-sm-12">			
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				
				
				 
				
				<div class="carousel-inner">
					<div class="item carousel-item active">
						<div class="row">
							<div class="col-sm-6">
								<div class="testimonial">
									<p>Love this gym! Workouts are always different and I love that it’s like family. Everyone works hard and it helps push you to work harder. I feel comfortable and always so helpful with any questions I may have and you can’t say that about many gyms these days. All around great place!</p>
								</div>
								<div class="media">
								
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Miranda P</b></div>
										
										</div>										
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="testimonial">
									<p>This gym is fantastic! The coaches are wonderful and go out of their way to teach each and every individual. Each and every member was very welcoming when I joined and knew everyone by name. I can't say much more than I highly recommend Delta Prime Fitness to anyone looking for a great environment to succeed!</p>
								</div>
								<div class="media">
									
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Val Sallijeva</b></div>
										
											
										</div>										
									</div>
								</div>
							</div>
						</div>			
					</div>
					<div class="item carousel-item">
						<div class="row">
							<div class="col-sm-6">
								<div class="testimonial">
									<p>Love love love Delta Prime! Super fun workouts that are never the same, a great support system and friends that are more like family. I never ever get bored, am always challenged, never judged and above all else am excited to go to the gym. Highly recommend!!!</p>
								</div>
								<div class="media">
									
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Courtney LeMahieu-Dunn</b></div>
										
										
										</div>										
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="testimonial">
									<p>I love the support from others in the class! It’s like another family! Classes are always different. It never gets boring and always challenging! Fun atmosphere</p>
								</div>
								<div class="media">
								
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Christine Penney</b></div>
											
											
										</div>										
									</div>
								</div>
							</div>
						</div>			
					</div>
					<div class="item carousel-item">
						<div class="row">
							<div class="col-sm-6">
								<div class="testimonial">
									<p>Great instructors that push you while still making sure you're using correct form..</p>
								</div>
								<div class="media">
									
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Mike Ahnen</b></div>
											
										</div>										
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="testimonial">
									<p>I absolutely love Delta Prime! I am a mom of three kids and there are many schedule options for me to work out. I would definitely recommend anyone!</p>
								</div>
								<div class="media">
									
									<div class="media-body">
										<div class="overview">
											<div class="name"><b>Jessica Alvarez</b></div>
								
										</div>										
									</div>
								</div>
							</div>
						</div>			
					</div>
                    	
				</div>
                <div class = "movebuttons">
								<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev"><img src = "leftarrow.png" height = "80px" alt = "left">
					<i class="fa fa-chevron-left"></i>
				</a>
				<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                    <img src = "rightarrow.jpg" height = "80px" alt = "right">
					<i class="fa fa-chevron-right"></i>
				</a>	
				<br><br><br>
                </div>
			</div>
		</div>
	</div>
</div>


  
<br>

      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<br><br>

        </body>





<?php
include 'footer1.php';
?>





    </HTML>
