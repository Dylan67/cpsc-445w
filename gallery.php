    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
  
      <link rel ="stylesheet" type = "text/css" href="lightbox.min.css">
      <script type="text/javascript" src="lightbox-plus-jquery.min.js"></script>

         

        </head>

        <body>
          <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 
    <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

            <div class="header"><h2>Gallery</h2></div>
          <div class="gallery">
              
               <a href = "gym.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gym.jpg"></a>
               <a href = "gallery1.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery1.jpg"></a>
               <a href = "gallery2.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery2.jpg"></a>
               <a href = "gallery3.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery3.jpg"></a>
                <a href = "gallery4.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery4.jpg"></a>
               <a href = "gallery5.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery5.jpg"></a>
               <a href = "gallery6.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery6.jpg"></a>
               <a href = "gallery7.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery7.jpg"></a>
               <a href = "gallery8.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery8.jpg"></a>
               <a href = "gallery9.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery9.jpg"></a>
                <a href = "gallery10.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery10.jpg"></a>
                <a href = "gallery11.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery11.jpg"></a>
                <a href = "gallery12.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery12.jpg"></a>
                <a href = "gallery13.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery13.jpg"></a>
                <a href = "gallery14.jpg" data-lightbox="mygallery" data-lightbox="mygallery"> <img src = "gallery14.jpg"></a>
              
            </div>
      
          

      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </body>



<?php
include 'footer.php';
?>







    </HTML>
