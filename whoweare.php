    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
  
      

         

        </head>

        <body>
      <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 

      <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

           <section class="gym">
            
            </section>
            <section class ="aboutdeltaprime">
            <h1>About Delta Prime Fitness</h1>
                <u><h3>History</h3></u>
            <p>Delta Prime Fitness was founded in 2018 by owner and head trainer, Michael Penney. His vision was to provide a training facility to help individuals “Become A Better You”. Delta Prime Fitness is not just your average workout facility, but built and based on its members and their needs. Each member has become part of a family, a part of the real purpose in health and fitness. Delta Prime has built a wide reputation for being a community-based facility that focuses on the health and wellbeing of each and every person. The moment you step into the facility there is an immediate sense of care and respect. </p>
            <u><h3>Our Facility</h3></u>
                <p>The facility is comprised of 2,400 sq. ft. of mostly free weights and core training tools. With its focus on abstract equipment and unique training methods, the experience is unlike itself. Delta Prime operates by appointment-only classes, semi-private training and private one-on-one training. The Boot Camp style classes are designed to promote weight loss, strength, flexibility, mobility and a healthier lifestyle. The Active Adult class for the senior population that emphasizes on an active lifestyle, training for strength, mobility, flexibility and developing a more independent lifestyle. Delta Prime also offers options for semi-private and private training one-on-one with one of the qualified trainers to help everyone meet their goals and gain the results they aspire. </p>
                <u><h3>Our Focus</h3></u>
            <p>Delta Prime Fitness instructs and promotes a healthier way of living through healthy nutrition habits. Focusing on real whole foods and consistency in training, we do not condone gimmicks, weight loss supplements or fad diets. The transformation to a new beginning of a healthier and happier you begins with wholesome lifestyle changes in food and exercise. Make a change for a better you today at Delta Prime Fitness.</p>
            
            </section>
            
            
            <section class = "map">
            <h1>Come Visit Us Today!</h1>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d785.6619429048178!2d-77.51526017078751!3d38.0319901987321!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x12837cc341090818!2sDelta+Prime+Fitness!5e0!3m2!1sen!2sus!4v1550448288432" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>            
            </section>

      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </body>



<?php
include 'footer.php';
?>






    </HTML>
