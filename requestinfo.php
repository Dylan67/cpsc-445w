    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
      

         

        </head>

        <body>
        <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 

   <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>



    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

         
      <div class="header"><h2>Request Information</h2></div>   
  <div class="container3">
 <script>
$("form").submit(function(event) {

   var recaptcha = $("#g-recaptcha-response").val();
   if (recaptcha === "") {
      event.preventDefault();
      alert("Please check the recaptcha");
   }
});
</script>
<script>
function show_alert() {
var x = document.getElementById("fname").value;
   alert("Thank you for submitting your request information form "+x+"! We will get back to you as soon as possible!");
}
</script>
<script>
    window.onload = function() {
    var $recaptcha = document.querySelector('#g-recaptcha-response');

    if($recaptcha) {
        $recaptcha.setAttribute("required", "required");
    }
};
    
    
</script>
  <form method="post" action="requestsent.php" onsubmit="return submitUserForm();">
    <div class="form-group">
      <label for="fname">First Name:</label>
      <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname" required>
    </div>
    <div class="form-group">
      <label for="lname">Last Name:</label>
      <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname" required>
    </div>
       <div class="form-group">
    <label for="email">Email Address:</label>
    <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" required>
  </div>
     <div class="form-group">
    <label for="phone">Phone Number:</label>
    <input type="text" class="form-control" id="phone" placeholder="Enter Phone Number" name="phone" required>
  </div>
   <div class="form-group">
    <label for="preference">Prefered Method of Contact:</label>
    <input type="text" class="form-control" id="preference" placeholder="Phone Call or Email" name="preference" required>
  </div>
      <div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
</div>
<div class="g-recaptcha" data-sitekey="6LeG6psUAAAAADlULxIH5PDP3Tru0ZrODmO6mH9y
"></div>
    <button type="submit" for="submit" class="btn" name="submit" >Submit</button>
  </form>
</div>
            <p></p>
<script>
function submitUserForm() {
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">This field is required.</span>';
        return false;
    }
    return true;
}
 
function verifyCaptcha() {
    document.getElementById('g-recaptcha-error').innerHTML = '';
}
</script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </body>





<?php
include 'footer.php';
?>





    </HTML>
