    <!DOCTYPE HTML>
    <HTML lang = "en">
        <head>
            <meta charset="utf-8">
            <meta name = "viewport" content = "width=device-width, initial-scale=1.0">

        <title>Delta Prime</title>
   <script src = "https://code.jquery.com/jquery-3.2.1.js">
            </script>  
            <script src="js/bootstrap.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <link rel="stylesheet" href="main.css">
  
      

         

        </head>

        <body>
         <div class="topnav" id="myTopnav">
          <img src="dplogo.jpg" height="100px;">
  <a href="index.php" class="active">Home</a>
  <a href="meettheteam.php">Meet the Team</a>
 
  <div class="dropdown">
    <button class="dropbtn">Contact Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="requestinfo.php">Request Information</a>
      <a href="index.php#calendar">View Class Schedule</a>
      <a href="socialmedia.php">Social Media</a>
    </div>
  </div> 
     <a href="gallery.php">Gallery</a>
  <a href="faq.php">FAQ</a>
    <a href="membership.php">Memberships</a>
     <div class="dropdown">
    <button class="dropbtn">About Us
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="whoweare.php">Who We Are</a>
      <a href="testimonials.php">Testimonials</a>
      <a href="ourclasses.php">Our Classes</a>
    </div>
  </div> 

    <?php
session_start();
$name = "Welcome " . $_SESSION['username'];

$html = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;
$users = <<<END






   <div class="dropdown">
    <button class="dropbtn">$name
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="workouts.php">Workouts</a>
      <a href="users.php">Users List</a>
      <a href="admin.php">Admin List</a>
      <a href="logout.php">Logout</a>
    </div>
  </div> 

END;

$login = <<<EOD

<a href="login.php">Login</a>

EOD;
$admin = "Admin123";
if ($_SESSION['admin'] == "admin" ){
    echo $users;
}
else if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {

echo $html;
}

else{
echo $login;

}
?>

    
    
    
    
    
    
    
    
    
    
    
    
    
     
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
    <script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

          
         <div class = "header1"><h1>Pricing</h1></div>
       <div class ="pricing">     
    <h3>Active Adult Group Class</h3>
            <p>To learn more about our active adult group classes click <a href = "ourclasses.php">here.</a></p>
    
               <table><tr><th>Description</th>
            <th>Price</th>
        </tr>
            <tr>
        <td>3 Times a Week</td>
        <td>$69/Per Month</td></tr>
            <tr><td>2 Times a Week</td>
                <td>$59/Per Month</td></tr></table>
            
        <h3>Group Fitness Training</h3>
            <p>To learn more about our group fitness training click <a href = "ourclasses.php">here.</a></p>
      <table><tr><th>Description</th>
            <th>Price</th>
        </tr>
            <tr>
        <td>10 Pack Group Training Session</td>
        <td>$115/Per Package</td></tr>
            <tr><td>5 Pack Group Training Sesssion</td>
        <td>$60/Per Package</td></tr>
            <tr><td>6 Month Bootcamp Contract</td>
                <td>$99/Per Month</td></tr>
            <tr><td>6 Month Unlimited Group Training Contract</td>
          <td>$119/Per Month</td></tr>
          <tr>
            <td>No contract unlimited group training</td>
              <td>$150/Per Month</td></tr></table>
            
            
      <h3>Small Group Personal Training</h3>
            <p>To learn more about our small group personal training click <a href = "ourclasses.php">here.</a></p>
      <table><tr><th>Description</th>
            <th>Price</th>
        </tr>
            <tr>
        <td>4 60 Minute Small Group Training Sessions</td>
        <td>$200/Per Package</td></tr>
            <tr><td>8 30 Minute Transformation Sessions </td>
        <td>$233/Per Package</td></tr>
            <tr><td>Personal Training</td>
                <td>$400/Per Month</td></tr>
            <tr><td>Personal Training 3 times a week</td>
                <td>$360/Per Month</td></tr>
          <tr>
            <td>Small Group 30 Minute Session</td>
            <td>$35/Per Session</td>
          </tr>
         <tr><td>Small Group 60 Minute Session</td>
          <td>$55/Per Session</td></tr> </table>
          
            </div>      
          
<br>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        </body>



<?php
include 'footer.php';
?>







    </HTML>
