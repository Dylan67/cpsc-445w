This is my capstone project that I completed my final semester of school. It is 
a website that I built for my friends gym. It was hosted on an AWS EC2 instance
and contained an AWS MySQL RDS. The website contains basic information about the 
gym as well as a member portal where they were able to sign up for classes and 
view workouts to complete at home.